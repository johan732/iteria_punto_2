/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author johan
 */
public class Solution {

    /**
     * Busca un número en un arreglo y retorna si lo encuentra
     *
     * @param A Array
     * @param n Número a buscar
     * @return true si lo encuentra, false si no lo encuentra
     */
    private boolean findN(int[] A, int n) {
        for (int i = 0; i < A.length; i++) {
            if (A[i] == n) {
                return true;
            }
        }
        return false;
    }

    /**
     * Verifica si se encuentran los número de un rango en un array (a,b)
     *
     * @param A Array
     * @param a min/max
     * @param b min/max
     * @return true, si encuentra algún número del rango en el array, false si
     * no lo encuentra
     */
    private boolean findRange(int[] A, int a, int b) {

        boolean encontrado = false;

        int min = a;
        int max = b;

        if (a > b) {
            min = b;
            max = a;
        }

        for (int i = (min + 1); i <= (max - 1); i++) {
            if (findN(A, i)) {
                encontrado = true;
                break;
            }
        }

        return encontrado;
    }

    /**
     * Retorna la distancia minima entre los valores de un arreglo con número
     * adyacentes
     *
     * @param A Array
     * @return -1 si la distancia es mayor a 1000000, -2 si no existen indices
     * adyacentes, distanciaMinima encontrada
     */
    public int solution(int[] A) {

        // Defimos la distancia mayor que el limite de distancia del condicional
        int distanciaMin = 200000000;
        // Para verificar si hay valores adyacentes en el array
        boolean adyacentes = false;

        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < A.length; j++) {

                // Verificamos que no sea el mismo indice a comparar en la matriz
                if (i != j) {
                    // Verificamos si hay valores adyacentes
                    if (!findRange(A, A[i], A[j])) {
                        adyacentes = true;
                    }
                    //Calculamos la distancia entre los pares adyacentes
                    if (adyacentes) {
                        // Calculamos la distancia entre los indices
                        int distancia = Math.abs(A[i] - A[j]);
                        //Verificamos siempre la menor distancia
                        if (distancia < distanciaMin) {
                            distanciaMin = distancia;
                        }
                    }

                }
            }
        }

        // Verificamos si no hay adyacentes 
        if (!adyacentes) {
            return -2; // Si no existen indices adyacentes
        }

        // Verificamos el condicional de distancia
        if (distanciaMin > 100000000) {
            return -1;
        }

        return distanciaMin;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author johan
 */
public class ClasePrincipal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Solution s = new Solution();
        // Matrices de prueba
        int[] A = {0, 3, 3, 7, 5, 3, 11, 1}; // abs(3-3)=0
        int[] B = {2, 4, 6, 8, 10, 3}; // abs(3-4) = 1
        int[] C = {-2, 8, 20, 64, 107, 900}; // abs(-2-8)=10
        int[] D = {-2, 15, 33, 21, 0, -87}; // abs(-2-0)=2
        int[] E = {1, 2, 3, 4, 5, 6, 7, 8}; // abs(1-2)=1
        int[] F = {9, 9, 9, 9, 9, 9, 9}; // abs(9-9)=0
        int[] G = {0, 150000000, 2000000000}; // abs(0-150000000)=-1

        System.out.println("Matriz A:" + s.solution(A));
        System.out.println("Matriz B:" + s.solution(B));
        System.out.println("Matriz C:" + s.solution(C));
        System.out.println("Matriz D:" + s.solution(D));
        System.out.println("Matriz E:" + s.solution(E));
        System.out.println("Matriz F:" + s.solution(F));
        System.out.println("Matriz G:" + s.solution(G));
    }

}
